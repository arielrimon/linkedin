from app import app
from routes.user_route import urls_blueprint

app.register_blueprint(urls_blueprint)