import redis

class RedisCachingService:
    def __init__(self):
        pass

    def increment(self, key):
        r = redis.Redis()
        r.incr(key)
