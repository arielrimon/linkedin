from flask import session

from dal.authorization_dal import AuthorizationDAL
from dal.user_dal import UserDal
from services.redis_caching_service import RedisCachingService

user_DAL = UserDal()
authorization_DAL = AuthorizationDAL()
redis_caching_service = RedisCachingService()

class UserService:
    def __init__(self):
        pass

    def add_user(self, username, password, job):
        is_used_username = user_DAL.get_user(username) is not None

        if(is_used_username):
            return False
        else:
            user_DAL.add_user(username, password, job)
            return True

    def get_user(self, username):
        redis_caching_service.increment(username)
        user = user_DAL.get_user(username)
        return user

    def remove_user(self, username):
        if not session.get('username'):
            return 'Only registered users can to remove users'

        if user_DAL.get_user(username) is None:
            return 'User not found'

        connected_user = user_DAL.get_user(session['username'])

        if not authorization_DAL.is_authorized_to_remove(connected_user.id):
            return 'Only authorized users can remove users'
        else:
            user_DAL.remove_user(username)
            return 'User {} has been removed'.format(username)

    def login(self, username, password):
        user = user_DAL.get_user(username)

        if user is None or user.password != password:
            return False
        else:
            session['username'] = username
            session.permanent = True
            return True

    def logout(self):
        session['username'] = None

