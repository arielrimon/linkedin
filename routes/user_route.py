from flask import session, Blueprint

from services.user_service import UserService

urls_blueprint = Blueprint('/', __name__,)
user_service = UserService()

@urls_blueprint.route('/add_user/<username>/<job>/<password>')
def add_user(username, job, password):
    if user_service.add_user(username, password, job):
        return 'Added new user successfully'
    else:
        return 'Failed to add new user'


@urls_blueprint.route('/remove_user/<username>')
def remove_user(username):
    return user_service.remove_user(username)


@urls_blueprint.route('/get_user/<username>')
def get_user(username):
    user = user_service.get_user(username)
    if user is None:
        return "User not found"
    msg = 'Id: {}</br>' \
          'Username: {}</br>' \
          'Job: {}'.format(user.id, user.username, user.job)
    return msg

#
@urls_blueprint.route('/login/<username>/<password>')
def login(username, password):
    result = user_service.login(username, password)
    if result:
        return 'Welcome {}'.format(session['username'])
    else:
        return 'The username and/or password you specified are not correct.'


@urls_blueprint.route('/logout')
def logout():
    user_service.logout()
    return 'Logout successfully'
